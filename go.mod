module git.sr.ht/~aw/flounder

go 1.15

require (
	git.sr.ht/~adnano/go-gemini v0.1.14-0.20210209215514-d2c70a33d5b1
	github.com/BurntSushi/toml v0.3.1
	github.com/gorilla/feeds v1.1.1
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/sessions v1.2.1
	github.com/kr/pretty v0.2.1 // indirect
	github.com/mattn/go-sqlite3 v1.14.6
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
	golang.org/x/sys v0.0.0-20210124154548-22da62e12c0c // indirect
	golang.org/x/term v0.0.0-20201210144234-2321bbc49cbf // indirect
	golang.org/x/time v0.0.0-20201208040808-7e3f01d25324
)
